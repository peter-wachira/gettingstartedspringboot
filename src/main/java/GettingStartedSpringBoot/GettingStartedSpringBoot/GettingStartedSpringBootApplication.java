package GettingStartedSpringBoot.GettingStartedSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GettingStartedSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(GettingStartedSpringBootApplication.class, args);
	}

}
