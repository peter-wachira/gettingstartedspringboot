# Simple SpringBoot API
#### April 8th 2020
#### By **peteHack**

## Description

```bash

A simple project that uses Springboot Framework to create an API that has CRUD functionalities 

```
## Technologies Used
[![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)](https://forthebadge.com)



## Launching the Application

```bash
1.Launch the project in Intellij 

```


## Contact Details and Documentation

```bash

You can reach me via my personal email pwachira900@gmail.com

```



## License

- This project is licensed under the MIT Open Source license Copyright (c) 2019. 

[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://forthebadge.com)
